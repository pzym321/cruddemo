package pang.demo.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;
import pang.demo.pojo.Product;


public interface ProductRepository extends MongoRepository<Product, String> {
}
