package pang.demo.services;

import pang.demo.pojo.Product;

import java.util.List;


public interface ProductService {

    List<Product> listAll();
    Product getById(String id);
    Product save(Product product);
    Product update(Product product);

    void delete(String id);

}
