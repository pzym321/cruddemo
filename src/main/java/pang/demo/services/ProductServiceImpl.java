package pang.demo.services;

import pang.demo.mapper.ProductMapper;
import pang.demo.pojo.Product;
import pang.demo.repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.ArrayList;
import java.util.List;

@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private ProductMapper productMapper;

    @Override
    public List<Product> listAll() {
        List<Product> products = new ArrayList<>();
        productRepository.findAll().forEach(products::add);
        return products;
    }

    @Override
    public Product getById(String id) {
        return productRepository.findById(id).orElse(null);
    }

    @Override
    public Product save(Product product) {
        productRepository.save(product);
        productMapper.insert(product);
        return product;
    }

    @Override
    public Product update(Product product) {
        productRepository.save(product);
        Example example = new Example(Product.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("_id",product.getId());
        productMapper.updateByExample(product,example);
        return product;
    }

    @Override
    public void delete(String id) {
        productRepository.deleteById(id);
        Product product = new Product();
        product.setId(id);
        productMapper.delete(product);
    }
}