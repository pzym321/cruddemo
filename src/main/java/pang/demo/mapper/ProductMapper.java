package pang.demo.mapper;

import pang.demo.pojo.Product;
import tk.mybatis.mapper.common.Mapper;

@org.apache.ibatis.annotations.Mapper
public interface ProductMapper extends Mapper<Product> {

}
