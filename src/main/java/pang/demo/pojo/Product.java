package pang.demo.pojo;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.Column;
import javax.persistence.Table;
import java.math.BigDecimal;

@Document
@Table
public class Product {
    @Id
    @Column(name = "p_id")
    private String _id;
    @Column(name = "p_description")
    private String description;

    @Column(name = "p_price")
    private BigDecimal price;
    @Column(name = "p_url")
    private String imageUrl;

    public String getId() {
        return _id;
    }

    public void setId(String _id) {
        this._id = _id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

}
